const axios = require('axios');

async function getAllSymbols(exchange) {
    const URLS = getUrls(exchange)
    return axios.get(`${URLS.ENDPOINT}${URLS.EXCHANGE_INFOS}`)
}
