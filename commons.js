
function getUrls(exchange) {
    const ENDPOINT = exchange === 'spot' ?
        'https://api.binance.com' :
        'https://fapi.binance.com'

    const KLINES = exchange === 'spot' ?
        '/api/v3/klines' :
        '/fapi/v1/klines'

    const PING = exchange === 'spot' ?
        '/api/v3/exchangeInfo' :
        '/fapi/v1/exchangeInfo'

    const EXCHANGE_INFOS = exchange === 'spot' ?
        '/api/v3/exchangeInfo' :
        '/fapi/v1/exchangeInfo'

    return {ENDPOINT, KLINES, PING, EXCHANGE_INFOS}
}

const INTERVALS = [
    {
        label: '1 Minute',
        value: '1m'
    },
    {
        label: '3 Minutes',
        value: '3m'
    },
    {
        label: '5 Minutes',
        value: '5m'
    },
    {
        label: '15 Minutes',
        value: '15m'
    },
    {
        label: '30 Minutes',
        value: '30m'
    },
    {
        label: '1 Hour',
        value: '1h'
    },
    {
        label: '2 Hours',
        value: '2h'
    },
    {
        label: '4 Hours',
        value: '4h'
    },
    {
        label: '6 Hours',
        value: '6h'
    },
    {
        label: '8 Hours',
        value: '8h'
    },
    {
        label: '12 Hours',
        value: '12h'
    },
    {
        label: '1 Day',
        value: '1d'
    },
    {
        label: '3 Days',
        value: '3d'
    },
    {
        label: '1 Week',
        value: '1w'
    },
    {
        label: '1 Month',
        value: '1M'
    }
]

function disableAllButtons() {
    document.getElementById('status-container').style.display = 'flex'
    let buttons = document.getElementsByTagName('button')
    for (let i = 0; i < buttons.length; i++) {
        const button = buttons[i];
        button.disabled = true
    }
}

function enableAllButtons() {
    let buttons = document.getElementsByTagName('button')
    for (let i = 0; i < buttons.length; i++) {
        const button = buttons[i];
        button.disabled = false
    }
}

function statusUpdater(status) {
    let color
    switch (status) {
        case 'In Progress':
            color = 'blue';
            break;
        case 'Error':
            color = 'red';
            break;
        case 'Done':
            color = 'green';
            break;
        case 'Paused':
            color = 'orange';
            break;
    }
    document.getElementById('status').innerHTML = `Status : <span style='color: ${color}'>${status}`
}