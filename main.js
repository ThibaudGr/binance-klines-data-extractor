const { app, BrowserWindow, dialog, ipcMain } = require('electron')

app.on('ready', ()=>{
    const win = new BrowserWindow({
        width: 800,
        height: 278,
        webPreferences: {
          nodeIntegration: true
        }
    })
    win.removeMenu()
    win.loadFile('index.html')
    //win.webContents.openDevTools()
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})

/* try {
    require('electron-reloader')(module)
  } catch (_) {} */