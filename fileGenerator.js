const fs = require('fs')
const limit = 1000

function extractData(symbol, interval, exchange) {
    const URLS = getUrls(exchange)

    disableAllButtons()
    if(!fs.existsSync('./data')){
        fs.mkdirSync('./data')
    }
    let data = []
    axios.get(`${URLS.ENDPOINT}${URLS.EXCHANGE_INFOS}`)
    .then(async response => {
        console.log(URLS.ENDPOINT, response.data.rateLimits);
        const requestRateLimit = response.data.rateLimits.find(el=>el.rateLimitType === 'REQUEST_WEIGHT').limit
        hasNext = true
        init = false
        currentWeigth = 0
        do {
            let promiseList = []
            
            let loopLength = init?50:1

            for (let i = 1; i <= loopLength; i++) {
                let params = ''
                if(init){
                    const lastDayTime = data[data.length-1][0]
                    const prevLastDayTime = data[data.length-2][0]
                    const endTime = data[data.length-1][0] - i * limit * (lastDayTime - prevLastDayTime)
                    params += `&endTime=${endTime}`
                    if(endTime < 0)
                        break;
                }
                else{
                    init = true
                }
                promiseList.push(axios.get(`${URLS.ENDPOINT}${URLS.KLINES}?symbol=${symbol}&interval=${interval}${params}&limit=${limit}`))
            }
            await Promise.all(promiseList)
            .then(async resolved=>{
                resolved.forEach(promise => {
                    data.push(...promise.data)
                    if(promise.data.length < limit)
                        hasNext = false
                });
                currentWeigth = resolved[resolved.length-1].headers['x-mbx-used-weight-1m']
                statusUpdater('In Progress')
                document.getElementById('currentWeigth').innerHTML = 'API Limits : ' + currentWeigth + '/' + requestRateLimit
                if(parseInt(currentWeigth)+60 >= requestRateLimit){
                    let hasToWait = true
                    let timePassed = 0
                    while (hasToWait){
                        let ping = await axios.get(`${URLS.ENDPOINT}${URLS.PING}`)
                        if(ping.headers['x-mbx-used-weight-1m'] > 10){
                            statusUpdater('Paused')
                            document.getElementById('currentWeigth').innerHTML = `Limit request rate almost reached, ${ping.headers['x-mbx-used-weight-1m']}/${requestRateLimit},  waiting reset since ${timePassed}s`
                            timePassed += 5
                            await new Promise(r => setTimeout(r, 5000))
                        }
                        else{
                            hasToWait = false
                        }
                    }
                }
            })
            .catch(error=>{
                console.log(error);
                statusUpdater('Error')
                hasNext = false
            })
        } while (hasNext);
        await fs.writeFile(`./data/${symbol}_${interval}_${exchange}.json`, JSON.stringify(data.reverse()), ()=>{})
        document.getElementById('currentWeigth').innerHTML = `${data.length} row gathered`
        data = []
        enableAllButtons()
        statusUpdater('Done')
    })
    .catch(error => {
        console.log(error);
        document.getElementById('currentWeigth').innerHTML = error
        statusUpdater('Error')
    });

}