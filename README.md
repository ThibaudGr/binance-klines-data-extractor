First version of the Binance Data Extractor Tool, it help get all klines from all time from binance and save in a json fileBinance Data Extractor Tool, get all klines from all time from binance and save in json

![](./attachments/demo.png)

## Format
Datas as save as a json with name formatted as symbol_interval_exchange.json (example : BTCUSDT_5m_future.json)

data format is
```
[
  [
    1499040000000,      // Open time
    "0.01634790",       // Open
    "0.80000000",       // High
    "0.01575800",       // Low
    "0.01577100",       // Close
    "148976.11427815",  // Volume
    1499644799999,      // Close time
    "2434.19055334",    // Quote asset volume
    308,                // Number of trades
    "1756.87402397",    // Taker buy base asset volume
    "28.46694368",      // Taker buy quote asset volume
    "17928899.62484339" // Ignore.
  ]
]
```



## Executables
https://www.dropbox.com/sh/nza1fxospmdrsr3/AABW0DM9bTzjcl8UFHUCJLTda?dl=0

## Dev
```
npm install
npm start
```
## Create apps
```
electron-packager . BinanceDataExtractor --all --out=out
```